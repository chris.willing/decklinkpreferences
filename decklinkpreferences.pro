#-------------------------------------------------
#
# Project created by QtCreator 2015-07-02T09:37:32
#
#-------------------------------------------------

# SPDX-License-Identifier: GPL-3.0-or-later

QT       += core

QT       -= gui

TARGET = decklinkpreferences
CONFIG   += console
CONFIG   -= app_bundle
DEFAULT_BMSDK_PATH = /usr/include/decklink

TEMPLATE = app

# Check whether BMSDK_PATH environment variable exists
_BMSDK_PATH = $$BMSDK_PATH
isEmpty(_BMSDK_PATH) {
  _BMSDK_PATH = $$DEFAULT_BMSDK_PATH
#  message("Using default SDK path ($$_BMSDK_PATH)")
} else {
  message("Using user defined SDK path ($$_BMSDK_PATH)")
}

INCLUDEPATH += $$_BMSDK_PATH

SOURCES += main.cpp \
        $$_BMSDK_PATH/DeckLinkAPIDispatch.cpp

HEADERS +=

LIBS += -ldl
